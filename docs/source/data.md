# Data documentation
Use this to document the data used in the project.
We recommend using [table-schemas](https://frictionlessdata.io/guide/#table-schema) to document data structures where possible.

Please also document:
- the data provider:
  - where did you get this data ?
  - who is responsible for providing it ?
  - When did you get it ?
- the data life-cycle:
  - refresh frequency
  - Storage location
- Data outputs
