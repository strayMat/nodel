```{include} readme.md
```

```{eval-rst}
.. toctree::
   :hidden:
   :maxdepth: 1

   readme
   Data <data>
   Notebooks <notebooks>
   CLI usage <cli-usage>
   License <license>
   Changelog <changelog>
```

```{eval-rst}
.. role:: bash(code)
   :language: bash
   :class: highlight
```
