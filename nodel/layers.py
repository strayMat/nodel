import torch 
import torch.nn as nn

class TanhFixedPointLayerTanHFixedLayer(nn.Module):
    def __init__(self, out_features, tol=1e-4, max_iter=50):
        super().__init__()
        self.linear = nn.Linear(out_features, out_features, bias=False)
        self.tol = tol
        self.max_iter = max_iter

    def forward(self, x):
        # initialize output z to be zero
        z = torch.zeros_like(x)
        self.iterations = 0
        # iterate until convergence
        while self.iterations < self.max_iter:
            z_next = torch.tanh(self.linear(z) +x)
            self.err = torch.norm(z -z_next)
            z = z_next
            if self.err < self.tol:
                break
        return z


class TanhNewtonLayer(nn.Module):
    def __init__(self, out_features, tol = 1e-4, max_iter=50):
        super().__init__()
        self.linear = nn.Linear(out_features, out_features, bias=False)
        self.tol = tol
        self.max_iter = max_iter

    def forward(self, x):
        z = torch.zeros_like(x)
        self.iterations = 0

        while self.iterations < self.max_iter:
            z_linear = self.linear(z) + x
            g = z - torch.tanh(z_linear)
            self.error = torch.norm(g)
            if self.err < tol:
                break

            # newton step
            J = torch.eye(z.shape[1])