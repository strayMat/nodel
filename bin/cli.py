#! /usr/bin/env python
import click
from dotenv import load_dotenv

from nodel.utils import hello

# see `.env` for requisite environment variables
load_dotenv()


@click.group()
def cli():
    pass


@cli.command()
@click.version_option(package_name="nodel")
def main() -> None:
    """node_tuto Main entrypoint"""
    click.secho(hello(), fg="green")


if __name__ == "__main__":
    cli()
