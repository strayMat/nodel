Name of the project
==============================

**Documentation**: [strayMat/has-sante/node_tuto](strayMat/has-sante/node_tuto)

**Source Code**: [strayMat/has-sante/node_tuto](strayMat/has-sante/node_tuto)

> 📝 **Note**
> Utilisez le français pour tout le contenu de ce projet
---

Overview
--------
- TODO

Features
--------
- TODO

Requirements
------------
- TODO

Usage
=====
- TODO
    - High-level usage overview
------------
- TODO
    - Step 0 description




Running The Notebook
--------------------

> 📝 **Note**
>  All following commands are relative to the project root directory and assume
> `make` is installed.
To facilitate your interacting with notebooks with the minimal amount of
friction, here are two suggested options, in order of simplicity:
### 1. Docker Container Jupyter Environment (recommended)

Run:
```shell script
# Uncomment below to run with corresponding options.
#export PORT=8888 # default value; change this value if you need to run the container on a different port
# Note: *any* value other than `false` will trigger an option
#export IS_INTERACTIVE_SESSION=true
#export BIND_MOUNT_APPLICATION_DIR_ON_CONTAINER=true
make docker-build-run
```

which will build and run the project container image
that launches a Jupyter notebook environment preloaded with all the production
dependencies on `127.0.0.1:8888`.

You can then navigate to the Jupyter notebook URL displayed on your console.

### 2. Locally via Poetry (development workflow)

Run:
 ```shell script
make provision-environment # Note: installs ALL dependencies!
poetry shell # Activate the project's virtual environment
jupyter notebook # Launch the Jupyter server
# TODO
```

Development
===========

> 📝 **Note**
>  For convenience, many of the below processes are abstracted away
>  and encapsulated in single [Make](https://www.gnu.org/software/make/) targets.


> 🔥 **Tip**
>  Invoking `make` without any arguments will display
>  auto-generated documentation on available commands.

Package and Dependencies Installation
--------------------------------------

Make sure you have Python 3.8+ and [poetry](https://python-poetry.org/)
installed and configured.

To install the package and all dev dependencies, run:
```shell script
make provision-environment
```

> 🔥 **Tip**
>  Invoking the above without `poetry` installed will emit a
>  helpful error message letting you know how you can install poetry.

Docker Container Image Building/Deployment Orchestration
--------------------------------------------------------

The following set of `make` targets orchestrate the project's container image
build and deploy steps:

```shell
docker-build        Build evamed container
docker-rm           Stop container forcefully (i.e., when keyboard interrupts are disabled)

docker-jupyter-run  Runs the Dockerfile with the jupyter entrypoint

docker-run          Runs the Dockerfile with the default entrypoint
docker-run-interactive Runs the Dockerfile with a bash entrypoint
```

Note that the project's container image is insulated from the implementation
details of the application's top-level setup and execution logic which falls
under the purview of the project's entrypoint script. As such, Dockerfile
modifications will generally only be necessary when updating non-Python
environment dependencies (Python dependency updates are automatically reflected
in new image builds via the `pyproject.toml` and `poetry.lock` files).

Testing
------------

We use [pytest](https://pytest.readthedocs.io/) for our testing framework.

To invoke the tests, run:

```shell script
make test
```

Code Quality
------------

We use [pre-commit](https://pre-commit.com/) for our code quality
static analysis automation and management framework.

To invoke the analyses and auto-formatting over all version-controlled files, run:

```shell script
make lint
```

> 🚨 **Danger**
>  CI will fail if either testing or code quality fail,
>  so it is recommended to automatically run the above locally
>  prior to every commit that is pushed.

### Automate via Git Pre-Commit Hooks

To automatically run code quality validation on every commit (over to-be-committed
files), run:

```shell script
make install-pre-commit-hooks
```

> ⚠️ Warning
>  This will prevent commits if any single pre-commit hook fails
>  (unless it is allowed to fail)
>  or a file is modified by an auto-formatting job;
>  in the latter case, you may simply repeat the commit and it should pass.

Documentation
--------------

```shell script
make docs-clean docs-html
```
